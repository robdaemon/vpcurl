# vpcurl

CURL bindings for Virtual Pascal OS/2

## Requirements

Install curl from yum:

```
yum install libcurl
```

## Building

Either from the command line:

```
vpc @build.cfg vpcurl
```

Or, in the Virtual Pascal IDE.

## License

Same as CURL. See LICENSE

