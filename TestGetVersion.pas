program TestGetVersion;

uses
  SysUtils, vpcurl;

var
   c: PCURL;
   res: CURLcode;
   v: pchar;

begin
  { In windows, this will init the winsock stuff }
  curl_global_init(CURL_GLOBAL_ALL);

  { get a curl handle }
  c := curl_easy_init;
  if c <> nil then
  begin
    WriteLn('curl initialized');

    v := curl_version;
    Write('version string length = ');
    WriteLn(StrLen(v));
    Write('version               = ');
    WriteLn(StrPas(v));
  end else begin
    WriteLn('failed during curl_easy_init');
  end;
end.

